FROM cloudron/base:1.0.0@sha256:147a648a068a2e746644746bbfb42eb7a50d682437cead3c67c933c546357617

MAINTAINER Authors name <support@cloudron.io>

RUN mkdir -p /app/code /app/data

WORKDIR /app/code

EXPOSE 8000

ENV OCVERSION=3.0.3.2

# configure apache
RUN rm /etc/apache2/sites-enabled/*
RUN sed -e 's,^ErrorLog.*,ErrorLog "|/bin/cat",' -i /etc/apache2/apache2.conf

RUN a2disconf other-vhosts-access-log

# configure mod_php
RUN a2enmod rewrite
RUN crudini --set /etc/php/7.2/apache2/php.ini PHP upload_max_filesize 256M && \
    crudini --set /etc/php/7.2/apache2/php.ini PHP upload_max_size 256M && \
    crudini --set /etc/php/7.2/apache2/php.ini PHP post_max_size 256M && \
    crudini --set /etc/php/7.2/apache2/php.ini PHP memory_limit 256M && \
    crudini --set /etc/php/7.2/apache2/php.ini PHP max_execution_time 200 && \
    crudini --set /etc/php/7.2/apache2/php.ini Session session.save_path /run/ampache/sessions && \
    crudini --set /etc/php/7.2/apache2/php.ini Session session.gc_probability 1 && \
    crudini --set /etc/php/7.2/apache2/php.ini Session session.gc_divisor 100

# opencart
RUN wget -O opencart.zip https://github.com/opencart/opencart/releases/download/${OCVERSION}/opencart-${OCVERSION}.zip && \
    unzip opencart.zip && \
    rm opencart.zip

# prepare directories for copying
RUN mv /app/code/upload/system/storage /app/code/upload/system/storage_orig && \
    mv /app/code/upload/image /app/code/upload/image_orig && \
    mv /app/code/upload/install /app/code/upload/install_orig && \
    mv /app/code/upload/admin/view /app/code/upload/admin/view_orig && \
    mv /app/code/upload/admin/controller/extension /app/code/upload/admin/controller/extension_orig && \
    mv /app/code/upload/admin/language /app/code/upload/admin/language_orig && \
    mv /app/code/upload/admin/model/extension /app/code/upload/admin/model/extension_orig && \
    mv /app/code/upload/catalog /app/code/upload/catalog_orig && \
    mv /app/code/upload/system/config /app/code/upload/system/config_orig && \
    mv /app/code/upload/system/library /app/code/upload/system/library_orig

# link directories & config
RUN ln -sf /app/data/image /app/code/upload/image && \
    ln -sf /app/data/config.php /app/code/upload/config.php && \
    ln -sf /app/data/admin/config.php /app/code/upload/admin/config.php && \
    ln -sf /app/data/htaccess /app/code/upload/.htaccess && \
    ln -sf /app/data/admin/view /app/code/upload/admin/view && \
    ln -sf /app/data/admin/controller/extension /app/code/upload/admin/controller/extension && \
    ln -sf /app/data/admin/language /app/code/upload/admin/language && \
    ln -sf /app/data/admin/model/extension /app/code/upload/admin/model/extension && \
    ln -sf /app/data/catalog /app/code/upload/catalog && \
    ln -sf /app/data/system/config /app/code/upload/system/config && \
    ln -sf /app/data/system/library /app/code/upload/system/library && \
    ln -sf /app/data/install /app/code/upload/install

RUN sed -i "s/'DIR_SYSTEM'.*/'DIR_SYSTEM', '\/app\/code\/upload\/system\/');/" /app/code/upload/install_orig/cli_install.php

RUN chown -R www-data.www-data /app/code /app/data

ADD opencart.conf /etc/apache2/sites-enabled/opencart.conf

RUN echo "Listen 8000" > /etc/apache2/ports.conf

ADD start.sh /app/

CMD [ "/app/start.sh" ]
