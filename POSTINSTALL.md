This application does not integrate with cloudron. Use the following credentials to login:

* User: `admin`
* Pass: `changeme`

**Please change the credentials immediately**
