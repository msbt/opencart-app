#!/bin/bash

set -eux

if [[ ! -f /app/data/config.php ]]; then
    echo "=> Detected first run"

    mkdir -p /app/data/admin/model /app/data/admin/controller /app/data/system/

    # copy config
    cp -r /app/code/upload/install_orig /app/data/install
    cp -r /app/code/upload/system/storage_orig /app/data/storage
    cp -r /app/code/upload/image_orig /app/data/image
    cp /app/code/upload/config-dist.php /app/data/config.php
    cp /app/code/upload/admin/config-dist.php /app/data/admin/config.php
    cp /app/code/upload/.htaccess.txt /app/data/htaccess

    # extension directories
    cp -r /app/code/upload/admin/view_orig /app/data/admin/view
    cp -r /app/code/upload/admin/controller/extension_orig /app/data/admin/controller/extension
    cp -r /app/code/upload/admin/language_orig /app/data/admin/language
    cp -r /app/code/upload/admin/model/extension_orig /app/data/admin/model/extension
    cp -r /app/code/upload/catalog_orig /app/data/catalog
    cp -r /app/code/upload/system/config_orig /app/data/system/config
    cp -r /app/code/upload/system/library_orig /app/data/system/library

    chmod 0755 /app/data/storage/cache/
    chmod 0755 /app/data/storage/download/
    chmod 0755 /app/data/storage/logs/
    chmod 0755 /app/data/storage/modification/
    chmod 0755 /app/data/storage/session/
    chmod 0755 /app/data/storage/upload/
    chmod 0755 /app/data/storage/vendor/
    chmod 0755 /app/data/image/
    chmod 0755 /app/data/image/cache/
    chmod 0755 /app/data/image/catalog/
    chmod 0755 /app/data/config.php
    chmod 0755 /app/data/admin/config.php

    # create database
    /usr/bin/php /app/code/upload/install/cli_install.php install --db_hostname ${MYSQL_HOST} --db_username ${MYSQL_USERNAME} --db_password ${MYSQL_PASSWORD} --db_database ${MYSQL_DATABASE} --db_driver mysqli --db_port ${MYSQL_PORT} --username admin --password changeme --email ${MAIL_FROM} --http_server ${APP_ORIGIN}/

    rm -rf /app/data/install

    sed -i "s/'DIR_APPLICATION'.*/'DIR_APPLICATION', '\/app\/code\/upload\/catalog\/');/" /app/data/config.php
    sed -i "s/'DIR_STORAGE'.*/'DIR_STORAGE', '\/app\/data\/storage\/');/" /app/data/config.php
    sed -i "s/'DIR_SYSTEM'.*/'DIR_SYSTEM', '\/app\/code\/upload\/system\/');/" /app/data/config.php

    sed -i "s/'DIR_APPLICATION'.*/'DIR_APPLICATION', '\/app\/code\/upload\/admin\/');/" /app/data/admin/config.php
    sed -i "s/'DIR_IMAGE'.*/'DIR_IMAGE', '\/app\/data\/image\/');/" /app/data/admin/config.php
    sed -i "s/'DIR_CATALOG'.*/'DIR_CATALOG', '\/app\/code\/upload\/catalog\/');/" /app/data/admin/config.php
    sed -i "s/'DIR_SYSTEM'.*/'DIR_SYSTEM', '\/app\/code\/upload\/system\/');/" /app/data/admin/config.php
    sed -i "s/'DIR_STORAGE'.*/'DIR_STORAGE', '\/app\/data\/storage\/');/" /app/data/admin/config.php

fi

chown -R www-data.www-data /app/data

# update mail & mysql info in case they changed
    sed -i "s/'DB_HOSTNAME'.*/'DB_HOSTNAME', '${MYSQL_HOST}');/" /app/data/config.php
    sed -i "s/'DB_USERNAME'.*/'DB_USERNAME', '${MYSQL_USERNAME}');/" /app/data/config.php
    sed -i "s/'DB_PASSWORD'.*/'DB_PASSWORD', '${MYSQL_PASSWORD}');/" /app/data/config.php
    sed -i "s/'DB_DATABASE'.*/'DB_DATABASE', '${MYSQL_DATABASE}');/" /app/data/config.php

    sed -i "s/'DB_HOSTNAME'.*/'DB_HOSTNAME', '${MYSQL_HOST}');/" /app/data/admin/config.php
    sed -i "s/'DB_USERNAME'.*/'DB_USERNAME', '${MYSQL_USERNAME}');/" /app/data/admin/config.php
    sed -i "s/'DB_PASSWORD'.*/'DB_PASSWORD', '${MYSQL_PASSWORD}');/" /app/data/admin/config.php
    sed -i "s/'DB_DATABASE'.*/'DB_DATABASE', '${MYSQL_DATABASE}');/" /app/data/admin/config.php

    mysql --user=${MYSQL_USERNAME} --password=${MYSQL_PASSWORD} --host=${MYSQL_HOST} ${MYSQL_DATABASE} <<< "UPDATE oc_setting SET value = 'smtp' WHERE oc_setting.key = 'config_mail_engine';"
    mysql --user=${MYSQL_USERNAME} --password=${MYSQL_PASSWORD} --host=${MYSQL_HOST} ${MYSQL_DATABASE} <<< "UPDATE oc_setting SET value = '${MAIL_SMTP_SERVER}' WHERE oc_setting.key = 'config_mail_smtp_hostname';"
    mysql --user=${MYSQL_USERNAME} --password=${MYSQL_PASSWORD} --host=${MYSQL_HOST} ${MYSQL_DATABASE} <<< "UPDATE oc_setting SET value = '${MAIL_SMTP_USERNAME}' WHERE oc_setting.key = 'config_mail_smtp_username';"
    mysql --user=${MYSQL_USERNAME} --password=${MYSQL_PASSWORD} --host=${MYSQL_HOST} ${MYSQL_DATABASE} <<< "UPDATE oc_setting SET value = '${MAIL_SMTP_PASSWORD}' WHERE oc_setting.key = 'config_mail_smtp_password';"
    mysql --user=${MYSQL_USERNAME} --password=${MYSQL_PASSWORD} --host=${MYSQL_HOST} ${MYSQL_DATABASE} <<< "UPDATE oc_setting SET value = '${MAIL_SMTP_PORT}' WHERE oc_setting.key = 'config_mail_smtp_port';"


echo "=> Run opencart"
APACHE_CONFDIR="" source /etc/apache2/envvars
rm -f "${APACHE_PID_FILE}"

exec /usr/sbin/apache2 -DFOREGROUND
